$(window).ready(function() {

	var publishForm = $("#publishForm");

	// make the title field validate as required
	$("#title", publishForm).attr("required","required");

	// Update the displayed value of input type: range
	$(".html5.range", publishForm).bind("input change", function(){
		$(this).next(".value").text(Number(this.value).toFixed(1));
	});
	$(".html5.range", publishForm).each(function(){
		$(this).next(".value").text(Number(this.value).toFixed(1));
	});

	// placeholder polyfill
	if ( ! Modernizr.input.placeholder) {
		$("[placeholder]", publishForm).bind("focus", function(event){
			if (this.value == $(this).attr("placeholder")) {
				this.value = "";
			}
			$(this).removeClass("placeholder");
		});
		$("[placeholder]", publishForm).bind("blur", function(event){
			if ( ! this.value) {
				this.value = $(this).attr("placeholder");
				$(this).addClass("placeholder");
			}
		});
		$("[placeholder]", publishForm).each(function(event){
			if ( ! this.value) {
				this.value = $(this).attr("placeholder");
				$(this).addClass("placeholder");
			}
		});
	}
	
	// force a protocol to urls (helps with validation & data prepping)
	$(".html5.url", publishForm).blur(function(){
		
		// sckip if no value
		if ( ! this.value || this.value == $(this).attr('placeholder')) return;

		// prefix "http://" to each url if its missing
		var newValue = [];
		var urls = this.value.split(",");
		for (var i = 0; i < urls.length; i++) {
			urls[i] = urls[i].replace(/^\s+|\s+$/g,"");
			if ( ! /^[a-z]{3,5}:\/\//.test(urls[i])) {
				newValue[i] = "http://" + urls[i];
			}
			else {
				newValue[i] = urls[i];
			}
		}
		this.value = newValue.join(",");
	});

	// tidy up the form pre-post
	$(publishForm).submit(function(event){
		// make sure url fields are empty ony submit
		$("input.url", this).each(function(){
			if (this.value == "http://") this.value = "";
		});

		if ( ! Modernizr.input.placeholder) {
			// clear fields with placeholder values
			$("input[placeholder]").each(function(){
				if (this.value == $(this).attr("placeholder")) {
					this.value = "";
				}
			});
		}
	});


	// Set up the yepnope (Modernizr.load) directives...
	Modernizr.load([
		{
			// Test if Input Range is supported using Modernizr
			test: Modernizr.inputtypes.range,
			// If ranges are not supported, load the slider script and CSS file
			nope: [
				// The slider CSS file
				urlThirdThemes+'html5_input/css/fd-slider.css',
				// Javascript file for slider
				urlThirdThemes+'html5_input/js/fd-slider.js'
			],
			callback: function(id, testResult) {
				// If the slider file has loaded then fire the onDomReady event
				if("fdSlider" in window && typeof (fdSlider.onDomReady) != "undefined") {
					try { fdSlider.onDomReady(); } catch(err) {}
				}
			}
		}
	]);
});
