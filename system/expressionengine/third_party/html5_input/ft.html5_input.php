<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * ExpressionEngine html5 Input Fieldstype
 *
 * @package		html5 Feilds
 * @category	Fieldtype
 * @description	Adds a fieldtype with applicable html5 types & attributes
 * @author		Toby Evans
 * @link		http://tobz.net.nz
 */

 class Html5_input_ft extends EE_Fieldtype {
	
	var $EE;
	var $info = array(
		'name'      => 'Html5 Input Fields',
		'version'   => '1.0.2'
	);
	var $has_array_data = TRUE;

	public function __construct()
	{
		$this->EE = get_instance();

		$this->EE->load->helper(array('form','array','url'));
		$this->EE->lang->loadfile('html5_input');

		// Only load this stuff if the cp class is available
		if (isset($this->EE->cp)) 
		{
			if ( ! defined("URL_THIRD_THEMES"))
			{
				define("URL_THIRD_THEMES", base_url() . "themes/third_party/");
			}
			$this->EE->cp->add_to_head('<link rel="stylesheet" href="'.URL_THIRD_THEMES.'html5_input/css/screen.css" />');
			$this->EE->cp->add_to_head('<script src="'.URL_THIRD_THEMES.'html5_input/js/modernizr.js"></script>');
			$this->EE->cp->add_to_head('<script>var urlThirdThemes = "'.URL_THIRD_THEMES.'";</script>');
			$this->EE->cp->add_to_head('<script src="'.URL_THIRD_THEMES.'html5_input/js/html5input.js"></script>');
		}
	}


	public function install()
	{
		return array();
	}


	/**
	* ====================================
	* Display the fieldtype settings form
	* ====================================
	*/
	public function display_settings($data='')
	{

		$prefix = 'html5';
		$extra = '';
		
		// max_length of the input
		$field_max_length = element('max_length', $data, 128);
		
		// field types
		$field_type_options = array(
			'text'=>lang('text'), 
			'textarea'=>lang('textarea'), 
			'email'=>lang('email'), 
			'url'=>lang('url'), 
			'tel'=>lang('tel'),
			'number'=>lang('number'),
			'range'=>lang('range'),
			// 'file'=>lang('file'),
		);
		
		// type
		$this->EE->table->add_row(
			lang('html5_field_type', 'html5_field_type'),
			form_dropdown('html5_field_type', $field_type_options, element('type', $data, 'text'), 'id="html5_field_type"').$extra
		);


		/**
		* ==================
		* Common attributes
		* ==================
		*/

		
		// maxlength
		$this->EE->table->add_row(
			array('class'=>'text textarea email url tel attrib', 'data'=>lang('html5_field_max_length', 'html5_field_max_length')),
			array('class'=>'text textarea email url tel attrib', 'data'=>form_input(array('id'=>'html5_field_max_length','name'=>'html5_field_max_length', 'size'=>4,'value'=>$field_max_length)))
		);

		// autofocus
		$this->EE->table->add_row(
			array('class'=>'text textarea email url number tel attrib', 'data'=>lang('html5_field_autofocus', 'html5_field_autofocus_yes')),
			array('class'=>'text textarea email url number tel attrib', 'data'=>
				form_radio(array('id'=>'html5_field_autofocus_yes','name'=>'html5_field_autofocus', 'value'=>'y', 'checked'=>element('autofocus', $data)=='y'?'checked':'')) . ' ' . form_label('Yes','html5_field_autofocus') . ' &nbsp; ' .
				form_radio(array('id'=>'html5_field_autofocus_no','name'=>'html5_field_autofocus', 'value'=>'', 'checked'=>element('autofocus', $data)=='y'?'':'checked')) . ' ' . form_label('No','html5_field_autofocus')
			)
		);

		// placeholder
		$this->EE->table->add_row(
			array('class'=>'text textarea email url tel search attrib', 'data'=>lang('html5_field_placeholder', 'html5_field_placeholder')),
			array('class'=>'text textarea email url tel search attrib', 'data'=>form_input(array('id'=>'html5_field_placeholder', 'name'=>'html5_field_placeholder', 'value'=>element('placeholder', $data))))
		);


		/**
		* =================
		* Text: attributes
		* =================
		*/

		// pattern
		$this->EE->table->add_row(
			array('class'=>'text attrib', 'data'=>lang('html5_field_pattern', 'html5_field_pattern')),
			array('class'=>'text attrib', 'data'=>form_input(array('id'=>'html5_field_pattern', 'name'=>'html5_field_pattern', 'value'=>element('pattern', $data))))
		);


		/**
		* =================
		* File: attributes
		* =================
		*/

		// accept
		$this->EE->table->add_row(
			array('class'=>'file attrib', 'data'=>lang('html5_field_accept', 'html5_field_accept')),
			array('class'=>'file attrib', 'data'=>form_input(array('id'=>'html5_field_accept', 'name'=>'html5_field_accept', 'value'=>element('accept', $data))))
		);

		// multiple
		$this->EE->table->add_row(
			array('class'=>'email url file attrib', 'data'=>lang('html5_field_multiple', 'html5_field_multiple_yes')),
			array('class'=>'email url file attrib', 'data'=>
				form_radio(array('id'=>'html5_field_multiple_yes','name'=>'html5_field_multiple', 'value'=>'y', 'checked'=>element('html5_field_multiple', $data)=='y'?'checked':'')) . ' ' . form_label('Yes','html5_field_multiple') . ' &nbsp; ' .
				form_radio(array('id'=>'html5_field_multiple_no','name'=>'html5_field_multiple', 'value'=>'', 'checked'=>element('html5_field_multiple', $data)=='y'?'':'checked')) . ' ' . form_label('No','html5_field_multiple')
			)
		);


		/**
		* ===================
		* Number: attributes
		* ===================
		*/
		
		// step
		$this->EE->table->add_row(
			array('class'=>'js_hide number range attrib', 'data'=>lang('html5_field_step', 'html5_field_step')),
			array('class'=>'js_hide number range attrib', 'data'=>form_input(array('id'=>'html5_field_step', 'type'=>'number', 'step'=>'0.1', 'name'=>'html5_field_step', 'value'=>element('step', $data, 1))))
		);

		// min
		$this->EE->table->add_row(
			array('class'=>'js_hide number range attrib', 'data'=>lang('html5_field_min', 'html5_field_min')),
			array('class'=>'js_hide number range attrib', 'data'=>form_input(array('id'=>'html5_field_min', 'type'=>'number', 'step'=>'0.1', 'name'=>'html5_field_min', 'value'=>element('min', $data))))
		);
		
		// max
		$this->EE->table->add_row(
			array('class'=>'js_hide number range attrib', 'data'=>lang('html5_field_max', 'html5_field_max')),
			array('class'=>'js_hide number range attrib', 'data'=>form_input(array('id'=>'html5_field_max', 'type'=>'number', 'step'=>'0.1', 'name'=>'html5_field_max', 'value'=>element('max', $data))))
		);

		// list
		$this->multi_item_row($data, $prefix);



		$this->text_direction_row($data, $prefix);

		$this->field_show_smileys_row($data, $prefix);
		$this->field_show_glossary_row($data, $prefix);
		$this->field_show_spellcheck_row($data, $prefix);
		$this->field_show_file_selector_row($data, $prefix);
		
		$this->EE->javascript->output('
		$(window).ready(function() {
			
			var html5_field_type = $("#html5_field_type");
			$(html5_field_type).change(function() {
				$(".attrib").hide();
				$(".attrib."+$(this).val().toLowerCase()).show();
			});

			$(".attrib").hide();
			$(".attrib."+$(html5_field_type).val().toLowerCase()).show();

		});
		');		
		
	}


	/**
	* =========================
	* Save the fields settings
	* =========================
	*/
	public function save_settings($data)
	{
		$settings = array(
			'site_id'=>element('site_id', $data),
			'type'=>$this->EE->input->post('html5_field_type'),
			'placeholder'=>$this->EE->input->post('html5_field_placeholder'),
			'max'=>$this->EE->input->post('html5_field_max'),
			'min'=>$this->EE->input->post('html5_field_min'),
			'step'=>$this->EE->input->post('html5_field_step'),
			'autofocus'=>$this->EE->input->post('html5_field_autofocus'),
			'max_length'=>$this->EE->input->post('html5_field_max_length'),
			'pattern'=>$this->EE->input->post('html5_field_pattern'),
			'multiple'=>$this->EE->input->post('html5_field_multiple'),
			'accept'=>$this->EE->input->post('html5_field_accept'),
			'list_populate_id'=>FALSE,
			'list_items'=>FALSE,
			'field_pre_populate' => $this->EE->input->post('html5_field_pre_populate'),
		);

		// save 'list' item settings
		if (in_array($settings['type'], array('text','email','url', 'tel'))) 
		{
			if ($this->EE->input->post('html5_field_pre_populate') == 'n') 
			{
				$settings['list_items'] = $this->EE->input->post('html5_field_list_items');
				
			}
			else
			{
				$pre_populate_id = explode('_', $this->EE->input->post('html5_field_pre_populate_id'));
				$settings['list_populate_id'] = array(
					'field_pre_channel_id' => $pre_populate_id['0'],
					'field_pre_field_id' => $pre_populate_id['1']
				);
				// print_r($settings['list_populate_id']);
			}
		}

		return $settings;
	}


	/**
	* ======================================
	* Display the field in the publish form
	* ======================================
	*/
	public function display_field($data)
	{

		switch (element('type', $this->settings))
		{
			case'url':
			case'email':
				if (element('multiple', $this->settings))
				{
					$data = str_replace('|', ',', $data);
				}
			break;
			case'tel':
				if ( ! element('placeholder', $this->settings))
				{
					$this->settings['placeholder'] = lang('tel_placeholder');
				}
			break;
		}


		$attributes = array(
			'name' => $this->field_name,
			'id' => $this->field_name,
			'value' => $data,
			'type' => element('type', $this->settings, 'text'),
			'class'=>'html5 '.element('type', $this->settings, 'text')
		);
		$extra = '';

		if (element('field_required', $this->settings)=='y') $attributes['required'] = 'required';
		if (element('placeholder', $this->settings)) $attributes['placeholder'] = element('placeholder', $this->settings);
		if (element('autofocus', $this->settings)) $attributes['autofocus'] = element('autofocus', $this->settings);
		if (element('max_length', $this->settings)) $attributes['max_length'] = element('max_length', $this->settings);
		
		if (element('max', $this->settings)) $attributes['max'] = element('max', $this->settings);
		if (element('min', $this->settings)) $attributes['min'] = element('min', $this->settings);
		if (element('step', $this->settings)) $attributes['step'] = element('step', $this->settings);
		
		if (element('pattern', $this->settings)) $attributes['pattern'] = element('pattern', $this->settings);
		
		if (element('multiple', $this->settings)) $attributes['multiple'] = element('multiple', $this->settings);
		if (element('accept', $this->settings)) $attributes['accept'] = element('accept', $this->settings);
		
		// add list & datalist if applicable
		if (element('list_items', $this->settings)) 
		{
			$attributes['list'] = $this->field_name.'_list';

			$options = explode("\n", element('list_items', $this->settings));
			$extra .= $this->datalist($options);
		}
		elseif (element('list_populate_id', $this->settings)) 
		{
			$attributes['list'] = $this->field_name.'_list';

			$extra .= $this->datalist($this->get_field_options($this->settings));
		}

		$output  = '';
		if ($attributes['type']=='textarea') {
			$output .= form_textarea($attributes);
		}
		// Add some extra stuff to show a Ranges' value.
		elseif ($attributes['type']=='range') {
			$output .= '<div class="range">'.form_input($attributes).'<span class="value">'.$attributes['value'].'</span></div>';
		}
		else {
			$output .= form_input($attributes);
		}

		$output .= $extra;


		return $output;

	}



	/**
	* ===============================================
	* Validate the data posted from the publish form
	* ===============================================
	*/
	public function validate($data)
	{

		$this->EE->load->library(array('form_validation'));

		switch (element('type', $this->settings)) 
		{
			case'email':
				if ($data && $this->settings['multiple'] && strpos($data, ',')) 
				{
					if ($data && ! $this->EE->form_validation->valid_emails($data))
					{
						return sprintf(lang('error_emails'));
					}
				}
				else 
				{
					if ($data && ! $this->EE->form_validation->valid_email($data))
					{
						return sprintf(lang('error_email'));
					}
				}
			break;
			case'number':
			case'range':
				if ($data && ! preg_match('/^[\d\.]+$/u', $data))
				{
					return sprintf(lang('error_number'));
				}
				if ($this->settings['min'] && (double) $this->settings['min'] > (double) $data) 
				{
					return sprintf(lang('error_min'), $this->settings['min']);
				}
				if ($this->settings['max'] && (double) $this->settings['max'] < (double) $data) 
				{
					return sprintf(lang('error_max'), $this->settings['max']);
				}
			break;
			case'url':
				if ($this->settings['multiple']) 
				{
					if ( ! $this->valid_urls($data))
					{
						return sprintf(lang('error_urls'));
					}
				}
				else 
				{
					if ( ! $this->valid_url($data))
					{
						return sprintf(lang('error_url'));
					}
				}
			break;
			case'tel':
				if ( ! $this->valid_tel($data))
				{
					return sprintf(lang('error_tel'));
				}
			break;
			case'text':
			case'textarea':
			default:
				
			break;
		}

		return TRUE;
	}


	/**
	* =====================
	* Prep data for saving
	* =====================
	*/
	public function save($data)
	{
		switch (element('type', $this->settings))
		{
			case'url':
				$this->EE->load->helper('url');
				$data = prep_url($data);

			case'email':
				if (element('multiple', $this->settings))
				{
					$data_array = explode(',', $data);
					$data = encode_multi_field($data_array);
				}
			break;
		}

		return $data;
	}


	/**
	* ================================
	* Render the data in the template
	* ================================
	*/
	public function replace_tag($data, $attributes=array(), $tag_data = FALSE)
	{
		
		$this->EE->load->helper('custom_field');
		$data_array = decode_multi_field($data);
		$autolink = !! $this->EE->TMPL->fetch_param('autolink') || element('autolink', $attributes);


		if ($tag_data) 
		{
			// hande tag data
			$variables = array();
			foreach ($data_array as $value) 
			{
				$variables[] = array(element('type', $this->settings)=>$value);
			}

			$data = $this->EE->TMPL->parse_variables($tag_data, $variables);
		}
		elseif (element('multiple', $this->settings)) {
			// handle none tag pair for fields with 'multiple' 
			$data = implode(', ', $data_array);
		}
		
		if ($autolink) {
			if (element('type',$this->settings)=='tel')
			{
				$data = $this->callto_linker($data, element('format', $this->settings));
			}
			else {
				$data = auto_link($data);
			}
		}

		return $data;
	}




	/**
	* ==========================
	* Helper methods start here
	* ==========================
	*/


	// --------------------------------------------------------------------
	
	function multi_item_row($data, $prefix = FALSE)
	{
		$prefix = ($prefix) ? $prefix.'_' : '';

		$this->EE->table->add_row(
			array('class'=>'text email url tel attrib top', 'data'=>
			lang('html5_list', $prefix.'multi_select_list_items').'<p class="field_format_option select_format">'.
				form_radio($prefix.'field_pre_populate', 'n', $data['field_pre_populate_n'], 'id="'.$prefix.'field_pre_populate_n"').NBS.
				lang('field_populate_manually', $prefix.'field_pre_populate_n').BR.
				form_radio($prefix.'field_pre_populate', 'y', $data['field_pre_populate_y'], 'id="'.$prefix.'field_pre_populate_y"').NBS.
				lang('field_populate_from_channel', $prefix.'field_pre_populate_y').
			'</p>'),

			array('class'=>'text email url tel attrib', 'data'=>'<p class="field_format_option select_format_n">'.
				lang('multi_list_items', $prefix.'multi_select_list_items').BR.
				lang('field_list_instructions').BR.
				form_textarea(array('id'=>$prefix.'field_list_items','name'=>$prefix.'field_list_items', 'rows'=>10, 'cols'=>50, 'value'=>element('list_items', $data))).
			'</p>
			<p class="field_format_option select_format_y">'.
				lang('select_channel_for_field', $prefix.'field_pre_populate_id').
				form_dropdown($prefix.'field_pre_populate_id', $data['field_pre_populate_id_options'], element('list_populate_id', $data), 'id="'.$prefix.'field_pre_populate_id"').
			'</p>')
		);
	
		$this->EE->javascript->click('#'.$prefix.'field_pre_populate_n', '$(".select_format_n").show();$(".select_format_y").hide();', FALSE);
		$this->EE->javascript->click('#'.$prefix.'field_pre_populate_y', '$(".select_format_y").show();$(".select_format_n").hide();', FALSE);
		
		// When this field becomes active for the first time - hit the option we need
		$this->EE->javascript->output('
			$("#ft_'.rtrim($prefix, '_').'_input").one("activate", function() {
				$("#'.$prefix.'field_pre_populate_'.$data['field_pre_populate'].'").trigger("click");
			});
		');
	}

	// --------------------------------------------------------------------
	
	private function get_field_options($data)
	{
		$field_options = array();
		
		if ($this->settings['field_pre_populate'] == 'n')
		{
			if ( ! is_array($this->settings['field_list_items']))
			{
				foreach (explode("\n", trim($this->settings['field_list_items'])) as $v)
				{
					$v = trim($v);
					$field_options[form_prep($v)] = form_prep($v);
				}
			}
			else
			{
				$field_options = $this->settings['field_list_items'];
			}
		}
		else
		{
			// We need to pre-populate this menu from an another channel custom field

			$this->EE->db->select('field_id_'.$this->settings['list_populate_id']['field_pre_field_id']);
			$this->EE->db->where('channel_id', $this->settings['list_populate_id']['field_pre_channel_id']);
			$pop_query = $this->EE->db->get('channel_data');

			if ($pop_query->num_rows() > 0)
			{
				foreach ($pop_query->result_array() as $prow)
				{
					$selected = ($prow['field_id_'.$this->settings['list_populate_id']['field_pre_field_id']] == $data) ? 1 : '';
					$pretitle = substr($prow['field_id_'.$this->settings['list_populate_id']['field_pre_field_id']], 0, 110);
					$pretitle = str_replace(array("\r\n", "\r", "\n", "\t"), " ", $pretitle);
					$pretitle = form_prep($pretitle);

					$field_options[form_prep($prow['field_id_'.$this->settings['list_populate_id']['field_pre_field_id']])] = $pretitle;
				}
			}
		}
		
		return $field_options;
	}



	/**
	* ==============================
	* Validation functions
	*  - I couln't figure out why I
	* can't extend the native class
	* ==============================
	*/


	/**
	 * Valid Url
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	private function valid_tel($str='')
	{
		if ( ! $str) return TRUE;
		return preg_match("/^((\+\d{2,3})*\s?)?(\(?\d{1,3}\)?\s)?\d{3,4}[\s-]{1}+\d{3,4}$/u", $str); // +12? (34)? 567-8910
	}

	// --------------------------------------------------------------------

	/**
	 * Valid Url
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	private function valid_url($str='')
	{
		if ( ! $str) return TRUE;
		return preg_match("/^(\w{3,5}):\/\/([\w\.\-_+\/]+\.\w{2,})\/?([\w\/\?=%#,-_+])+$/u", $str);
	}

	// --------------------------------------------------------------------

	/**
	 * Valid Urls
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	private function valid_urls($str='')
	{
		if (strpos($str, ',') === FALSE)
		{
			return $this->valid_url(trim($str));
		}

		foreach(explode(',', $str) as $url)
		{
			if (trim($url) != '' && $this->valid_url(trim($url)) === FALSE)
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Datalist
	 * 
	 * @param array
	 * @return string
	 */
	private function datalist($options=array())
	{
		$output = '<datalist id="'.$this->field_name.'_list'.'">';
		foreach ($options as $option) {
			if ( ! $option) continue;
			$output .= "	<option>$option</option>";
		}
		$output .= '</datalist>';

		return $output;
	}

	// --------------------------------------------------------------------

	/**
	 * 
	 * 
	 * 
	 */
	public function callto_linker($str='', $format='')
	{
		if (preg_match_all('/((\+\d{2,3})*\s?)?(\(?\d{1,3}\)?\s)?\d{3,4}[\s-]{1}+\d{3,4}/ui', $str, $matches))
		{
			for ($i = 0; $i < count($matches['0']); $i++)
			{
				$str = str_replace($matches[0][$i], '<a href="callto:'.str_replace(' ', '', $matches[0][$i]).'">'.$matches[0][$i].'</a>', $str);
			}
		}

		return $str;
	}



	/**
	* ===================
	* Add Matrix support
	* ===================
	*/

	public function display_cell_settings($data = '') {

	}

	public function save_cell_settings($data='')
	{
		# code...
	}

	public function validate_cell($data='')
	{
		# code...
	}

	public function save_cell($data='')
	{
		# code...
	}

 }

 // END CLASS

/* End of file ft.html5_input.php */