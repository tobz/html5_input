<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(
	'html5_field_max_length' => 'Max Length',
	'html5_field_type' => 'Input Type',
	'html5_field_pattern' => 'Pattern',
	'html5_field_min' => 'Minimum Number Allowed',
	'html5_field_max' => 'Maximum Number Allowed',
	'html5_field_step' => 'Increment Number by',
	'html5_field_placeholder' => 'Placeholder Text',
	'html5_field_autofocus' => 'Autofocus',
	'html5_field_multiple' => 'Allow Multiple Values',
	'html5_field_accept' => 'Allowed Mime Types (eg: image/*, video/mp4, etc...)',
	'html5_list' => 'Prefill Items',
	'text' => 'Text',
	'textarea' => 'Textarea',
	'email' => 'Email',
	'url' => 'Url',
	'range' => 'Range',
	'number' => 'Number',
	'tel' => 'Phone Number',
	'tel_placeholder' => '+12 (3) 456 789',
	'file' => 'File',
	'error_emails' => 'Must be a comma separated list of valid emails',
	'error_email' => 'Must be a valid email',
	'error_number' => 'Must be a number',
	'error_min' => 'Must be greater than or equal to %d',
	'error_max' => 'Must be less than or equal to %d',
	'error_urls' => 'Must be a comma separated list of valid urls',
	'error_url' => 'Must be a valid url',
	'error_tel' => 'Must be a valid Phone Number: +12 (3) 456 8790',
);